/* global expect, test */
import React from 'react';
import { validate } from '../'

const goodInput = {
  fullname: 'Some person',
  email: 'test@test.com',
  emailConfirmation: 'test@test.com',
}

describe('Validate', () => {
  it('returns an empty array and hasErrors is false if we have no errors', () => {
    expect(validate(goodInput)).toEqual({
      errors: [],
      hasErrors: false,
    })
  })

  it('requires a email and email confirmation', () => {
    const input = Object.assign({}, goodInput, { email: '' })
    expect(validate(input)).toEqual({
      errors: [
        { "error": "Please enter a valid email", "field": "email", },
      ],
      hasErrors: true
    })
  })

  it('requires a valid email and email confirmation', () => {
    const input = Object.assign({}, goodInput, { email: 'badinput', emailConfirmation: 'badinput' })
    expect(validate(input)).toEqual({
      errors: [
        { "error": "Please enter a valid email", "field": "email", },
        { "error": "Please enter a valid email confirmation", "field": "emailConfirmation", },
      ],
      hasErrors: true
    })
  })

  it('requires a valid name', () => {
    const input = Object.assign({}, goodInput, { fullname: '' })
    expect(validate(input)).toEqual({
      errors: [
        { "error": "Your full name is required", "field": "fullname", },
      ],
      hasErrors: true
    })
  })
});

import { css } from 'styled-components'

export const sizes = {
  margin: '2rem',
  padding: '2rem',
  main: '77vh',
  mobile: '67.5vh',
  headerAndFooter: '5vh',
}

export const colors = {
  color: '#ecf0f1',
  primaryText: '#333',
  background: '#34495e',
  primary: '#ecf0f1',
  primaryButton: '#ecf0f1',
  secondary: '#2980b9',
  warning: '#f39c12',
  error: '#c0392b',
  success: '#16a085',
}

export const mediaQueries = {
  mobile: (...args) => css`
    @media (max-width: 640px) {
      ${ css(...args) }
    }
  `
}

export default {
  sizes, colors, mediaQueries
}

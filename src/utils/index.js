import { isEmail, isEmpty } from 'validator';

export function validate({ email, emailConfirmation, fullname }){
  let errors = []
  if (isEmpty(fullname)) {
    errors.push({ field: 'fullname', error: 'Your full name is required'});
  }
  if (!isEmpty(fullname) && fullname.length < 3) {
    errors.push({ field: 'fullname', error: 'Your name is too short'});
  }
  if (isEmpty(email) || !isEmail(email)) {
    errors.push({ field: 'email', error: 'Please enter a valid email'});
  }
  if (isEmpty(emailConfirmation) || !isEmail(emailConfirmation)) {
    errors.push({ field: 'emailConfirmation', error: 'Please enter a valid email confirmation'});
  }
  if (isEmail(email) && email !== emailConfirmation) {
    errors.push({ field: 'emailConfirmation', error: 'Email fields should match'});
  }
  return { errors, hasErrors: !!errors.length }
}

/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import ContentPane from '../'

describe('ContentPane', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <ContentPane>
        <h1>Heading</h1>
        <p>Page content</p>
      </ContentPane>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { sizes, colors, mediaQueries } from '../../utils/styles'

const Main = styled.main`
  height:${sizes.main};
  background:${colors.background};
  color:${colors.color};
  display:flex;
  justify-content: center;
  align-content: center;
  align-items: center;
  ${mediaQueries.mobile`
    height:${sizes.mobile};
  `}
`

const ContentPane = ({ children }) =>
  <Main>
    {children}
  </Main>

ContentPane.propTypes = {
  children: PropTypes.node.isRequired,
}

export default ContentPane

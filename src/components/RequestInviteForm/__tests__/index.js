/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import RequestInviteForm from '../'

const fields = {
  fullname: 'tester 123',
  email: 'test@test.com',
  emailConfirmation: 'test@test.com',
}

describe('RequestInviteForm', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <RequestInviteForm
        hasErrors={false}
        errors={[]}
        fields={fields}
        handleSubmit={() => {}}
        handleToggleModal={() => {}}
        handleUpdateField={() => {}}
      />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })

  it('can render with errors', () => {
    const component = renderer.create(
      <RequestInviteForm
        hasErrors={true}
        errors={[{ field: 'email', error: 'Email field is required'}]}
        fields={fields}
        handleToggleModal={() => {}}
        handleSubmit={() => {}}
        handleUpdateField={() => {}}
      />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })

  it('can render submitting screen', () => {
    const component = renderer.create(
      <RequestInviteForm
        hasErrors={false}
        errors={[]}
        fields={fields}
        submitting={true}
        handleToggleModal={() => {}}
        handleSubmit={() => {}}
        handleUpdateField={() => {}}
      />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })

  it('can render success screen', () => {
    const component = renderer.create(
      <RequestInviteForm
        hasErrors={false}
        errors={[]}
        fields={fields}
        submitted={true}
        handleToggleModal={() => {}}
        handleSubmit={() => {}}
        handleUpdateField={() => {}}
      />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

import React from 'react'
import PropTypes from 'prop-types'
import { compose, branch, renderComponent } from 'recompose'
import styled from 'styled-components'
import { sizes, colors } from '../../utils/styles'
import Button, { SecondaryButton } from '../Button'
import { Input, FieldSet } from '../Form'
import { SectionHeading } from '../Typography'

const Container = styled.div``

const Notification = styled.div`
  font-weight:bold;
  padding:0.5rem;
  color:${colors.color};
`

const Warning = styled(Notification)`
  background:${colors.warning};
`
const Error = styled(Notification)`
  background:${colors.error};
`
const Success = styled(Notification)`
  background:${colors.success};
  margin:${sizes.margin} 0;
  padding:2rem;
`

const SuccessScreen = ({ handleToggleModal }) =>
  <div>
    <SectionHeading>All done!</SectionHeading>
    <Success>
      We will be in touch shortly!
      <span role="img" aria-label="rocket">🚀</span>
    </Success>
    <Button type={'button'} onClick={handleToggleModal}>Ok</Button>
  </div>

const FieldErrors = ({ errors }) => {
  const messages = errors.length
    ? <Error><ul>{errors.map(err => <li key={err.error}>{err.error}</li>)}</ul></Error>
    : null
  return messages
}

const Form = ({
  touched,
  submitting,
  errors,
  apiError,
  hasErrors,
  fields: { email, emailConfirmation, fullname },
  handleSubmit, handleUpdateField, handleToggleModal
}) =>
  <Container>
    <SectionHeading>Request an invite</SectionHeading>
    <FieldSet>
      <div>
        <Input
          name={'fullname'}
          placeholder={'Full name'}
          value={fullname}
          onChange={(e) => handleUpdateField({ field: 'fullname', value: e.currentTarget.value })}
        />
      </div>
      <FieldErrors errors={errors && errors.filter(f => f.field === 'fullname')} />
    </FieldSet>
    <FieldSet>
      <div>
        <Input
          name={'email'}
          placeholder={'Email'}
          value={email}
          onChange={(e) => handleUpdateField({ field: 'email', value: e.currentTarget.value })}
        />
      </div>
      <FieldErrors errors={errors && errors.filter(f => f.field === 'email')} />
    </FieldSet>
    <FieldSet>
      <div>
        <Input
          name={'emailConfirmation'}
          placeholder={'Confirm email'}
          value={emailConfirmation}
          onChange={(e) => handleUpdateField({ field: 'emailConfirmation', value: e.currentTarget.value })}
        />
      </div>
      <FieldErrors errors={errors && errors.filter(f => f.field === 'emailConfirmation')} />
    </FieldSet>
    <FieldSet>
      {!submitting && <Button disabled={!!hasErrors || !touched} type={'submit'} onClick={handleSubmit}>Send</Button>}
      {!submitting && <SecondaryButton type={'button'} onClick={handleToggleModal}>Close</SecondaryButton> }
      {submitting && <Warning><p>Submitting request...</p></Warning>}
    </FieldSet>
    {apiError && <Error>{apiError}</Error>}
  </Container>

Form.propTypes = {
  errors: PropTypes.arrayOf(PropTypes.shape({
    field: PropTypes.string,
    error: PropTypes.string,
  })).isRequired,
  hasErrors: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleUpdateField: PropTypes.func.isRequired,
}

const enhance = compose(
  branch(
    props => props.submitted,
    renderComponent(SuccessScreen),
    Component => Component,
  )
)

export default enhance(Form)

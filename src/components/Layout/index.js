import styled from 'styled-components'
import { sizes } from '../../utils/styles'

export const Centered = styled.div`
  text-align: center;
  margin: ${sizes.margin};
`

const Layout = styled.div`
  display:flex;
  flex-direction: column;
  align-content: space-between;
  justify-content: space-between;
  min-height:100%;
  font-weight:100;
  font-size:1rem;
`

export default Layout

/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import Layout, { Centered } from '../'

describe('Layout', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Layout>
        <h1>This is the page heading</h1>
      </Layout>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

describe('Centered', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Centered>
        <h1>This is the page heading</h1>
      </Centered>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

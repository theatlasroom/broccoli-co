import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { sizes, colors } from '../../utils/styles'

const StyledHeader = styled.header`
  height:${sizes.headerAndFooter};
  padding: ${sizes.padding};
  background: ${colors.primary};
  color: ${colors.primaryText};
`

const Header = ({ title, children }) =>
  <StyledHeader>
    {children}
  </StyledHeader>

Header.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Header

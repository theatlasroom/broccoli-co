/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import Header from '../'

describe('Header', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Header>
        <h1>This is the page heading</h1>
      </Header>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

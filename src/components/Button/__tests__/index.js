/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import Button from '../'

describe('Button', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Button onClick={() => {}}>
        <span role='img' aria-label='ftw!'>⚡️</span>
      </Button>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

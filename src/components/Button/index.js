import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { sizes, colors } from '../../utils/styles'

const StyledButton = styled.button`
  cursor: pointer;
  border: none;
  text-transform: uppercase;
  padding:calc(${sizes.padding}/2) ${sizes.padding};
  color: ${colors.primaryText};
  background-color: ${colors.primaryButton};
  font-size: 1.25em;
  text-transform: capitalize;
`

const Button = ({ type, disabled, onClick, children, ...rest }) => {
  const isDisabled = disabled ? { disabled: 'disabled'} : {}
  return (
    <StyledButton {...isDisabled} type={type} onClick={onClick} {...rest}>
      {children}
    </StyledButton>
  )
}

Button.defaultPropts = {
  type: 'button',
  disabled: false,
}

Button.propTypes = {
  type: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
}

export default Button
export const SecondaryButton = styled(Button)`
  background:${colors.background};
  color:${colors.color};
`

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledCta = styled.section`
  width: 40em;
  display:flex;
  flex-direction: column;
`

const Cta = ({ children }) => <StyledCta>{children}</StyledCta>

Cta.propTypes = {
  children: PropTypes.node.isRequired
}

export default Cta

/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import CallToAction from '../'

describe('CallToAction', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <CallToAction>This is a call to action</CallToAction>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

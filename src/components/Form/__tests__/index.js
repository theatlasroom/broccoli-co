/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import { Input, FieldSet } from '../'

const inputProps = {
  placeholder: 'Placeholder text',
  type: 'text',
}

const emailProps = {
  placeholder: 'Email field',
  type: 'email',
}

describe('Input', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Input {...inputProps} />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })

  it('can render email field without crashing', () => {
    const component = renderer.create(
      <Input {...emailProps} />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

describe('FieldSet', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <FieldSet />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

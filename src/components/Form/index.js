import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { sizes } from '../../utils/styles'

export const FieldSet = styled.fieldset`
  width:100%;
  margin:calc(${sizes.margin}/2) 0;
`

const StyledInput = styled.input`
  width:90%;
  padding:5%;
  font-size: 1rem;
`

export const Input = (props) =>
  <StyledInput {...props} />

Input.defaultProps = {
  type: 'text',
  placeholder: '',
  value: '',
}

Input.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
}

/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import Footer from '../'

describe('Footer', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Footer>
        <small>This is the page footer</small>
      </Footer>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

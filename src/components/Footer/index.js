import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { sizes, colors } from '../../utils/styles'

const StyledFooter = styled.footer`
  height:${sizes.headerAndFooter};
  text-align: center;
  padding: ${sizes.padding};
  font-size:12pt;
  background: ${colors.primary};
  color: ${colors.primaryText};
  font-weight:normal;
`

const Footer = ({ children }) =>
  <StyledFooter>
    {children}
  </StyledFooter>

Footer.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Footer

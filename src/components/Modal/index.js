import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { colors, sizes, mediaQueries } from '../../utils/styles'

const centerFlex = `
  display:flex;
  justify-content: center;
  align-content: center;
`

const StyledModal = styled.aside`
  top:0;
  position:absolute;
  height:100%;
  width:100%;
  background:rgba(0,0,0, 0.5);
  ${centerFlex};
  ${mediaQueries.mobile`
    font-size:14pt;
    top:0;
    height:100%;
    width:100%;
  `}
`

const StyledModalContent = styled.section`
  position: relative;
  top:20%;
  height:75%;
  width:20rem;
  ${mediaQueries.mobile`
    font-size:14pt;
    top:0;
    height:100%;
    width:100%;
  `}
`

const StyledModalInner = styled.div`
  position:relative;
  border:solid 2px black;
  background:${colors.background};
  color:${colors.color};
  max-height:30rem;
  width:100%;
  padding:${sizes.padding};
  ${mediaQueries.mobile`
    top:0;
    width:auto;
  `}
`

const Modal = ({ children }) =>
  <StyledModal>
    <StyledModalContent>
      <StyledModalInner>
        {children}
      </StyledModalInner>
    </StyledModalContent>
  </StyledModal>

Modal.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Modal

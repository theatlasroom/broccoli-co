/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import Modal from '../'

describe('Modal', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Modal>
        <h1>Modal heading</h1>
        <p>Modal content</p>
      </Modal>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

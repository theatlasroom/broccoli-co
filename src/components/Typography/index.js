import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { mediaQueries } from '../../utils/styles'

const Alpha = styled.h1`
  font-size:3rem;
  line-height:3rem;
  font-weight:bold;
  margin-bottom: 1rem;
  ${mediaQueries.mobile`
    font-size:2rem;
    line-height:2rem;
  `}
`

const Beta = styled.h2`
  font-size:1.75rem;
  line-height: 1.75rem;
  font-weight:100;
  font-style: italic;
  ${mediaQueries.mobile`
    font-size:1.25rem;
    line-height:1.25rem;
  `}
`

const StyledText = styled.p`
  font-size:1rem;
`

const StyledHeading = styled.h1`
  text-align: left;
  font-weight: bold;
  font-size:1.5rem;
  line-height: 4.5rem;
`

export const Heading = ({ children }) => <Alpha>{children}</Alpha>
export const SectionHeading = ({ children }) => <StyledHeading>{children}</StyledHeading>
export const Subheading = ({ children }) => <Beta>{children}</Beta>
export const Text = ({ children }) => <StyledText>{children}</StyledText>

export const Hero = ({ title, tagline }) =>
  <section>
    <Heading>{title}</Heading>
    <Subheading>{tagline}</Subheading>
  </section>

Hero.propTypes = {
  title: PropTypes.string.isRequired,
  tagline: PropTypes.string.isRequired,
}

/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import { Heading, Text, Hero, Subheading } from '../'

describe('Heading', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Heading>heading</Heading>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

describe('Subheading', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Subheading>Subheading</Subheading>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

describe('Text', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Text>Body text</Text>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

describe('Hero', () => {
  it('can render without crashing', () => {
    const component = renderer.create(
      <Hero title={'Hero text'} tagline={'tagline'} />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

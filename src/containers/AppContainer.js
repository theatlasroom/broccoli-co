// Wraps the app to manage state and interactions
import React from 'react'
import { compose, lifecycle, withHandlers } from 'recompose'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actions from '../state/actions'
import { requestSignup } from '../state/api'
import Layout from '../components/Layout'

const AppContainer = ({ children, initialized, ...rest }) =>
  initialized
    ? <Layout>
      {React.cloneElement(children, rest)}
    </Layout>
    : null

function handleToggleModal({ actions }){
  actions.toggleModal()
}

function handleUpdateField({ actions }, { field, value }){
  actions.updateFormField({ [field]: value })
  actions.validateFields()
}

function handleSubmit({ actions, email, fullname }){

  return (
    new Promise(
      (resolve, reject) => {
        actions.requestSignup({ submitting: true });
        requestSignup({ resolve, reject, email, fullname })
      },
    )
    .then((params) => {
      actions.requestSignupSuccess({ submitting: false, submitted: true })
    })
    .catch(err => {
      actions.requestSignupError({ submitting: false, apiError: err })
    })
  );
}

const enhance = compose(
  lifecycle({
    componentWillMount(){
      this.props.actions.initialize()
    },
    componentDidMount(){
      this.props.actions.initializeSuccess()
    },
  }),
  withHandlers({
    handleToggleModal: props => () => handleToggleModal(props),
    handleSubmit: props =>  () => handleSubmit(props),
    handleUpdateField: props => ({ field, value }) => handleUpdateField(props, { field, value }),
  }),
)

AppContainer.propTypes = {
}

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

const EnhancedAppContainer = connect(mapStateToProps, mapDispatchToProps)(
  enhance(AppContainer)
)

export default EnhancedAppContainer

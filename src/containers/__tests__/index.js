/* global expect, test */
import React from 'react';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import { initialState } from '../../state/rootReducer'
import AppContainer from '../AppContainer'

const middlewares = []
const mockStore = configureStore(middlewares)


const defaultState = Object.assign({}, initialState)

const handlers = {}

describe('AppContainer', () => {
  it('can render with no props without crashing', () => {
    const component = renderer.create(
      <Provider store={mockStore(defaultState)}>
        <AppContainer>
          <h1>My App</h1>
        </AppContainer>
      </Provider>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  })
});

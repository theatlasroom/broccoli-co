import React from 'react';
import { Provider } from 'react-redux'
import AppContainer from './containers/AppContainer'
import Header from './components/Header'
import Footer from './components/Footer'
import ContentPane from './components/ContentPane'
import { Hero, Heading, Text } from './components/Typography'
import Button from './components/Button'
import Modal from './components/Modal'
import CallToAction from './components/CallToAction'
import RequestInviteForm from './components/RequestInviteForm'
import createStore from './state/store'
import { Centered } from './components/Layout'

const baseStore = createStore()


const brand = 'Broccoli & Co.'
const footerContent = 'Made with ❤️, a bit of 🏓 and lots of 🍻 in (not so) sunny Melbourne.'
const title = 'A better way to enjoy everyday.'
const tagline = 'Be the first to know when we launch 🔥'

const Content = ({
  modalOpen,
  initialized,
  fullname, email, emailConfirmation,
  errors, hasErrors, apiError,
  touched,
  submitting,
  submitted,
  ...props
}) => {
  return [
    <Header key={'header'}>
      <Heading>
        {brand}
      </Heading>
    </Header>,
    <ContentPane key={'main'}>
      <CallToAction>
        <Centered>
          <Hero title={title} tagline={tagline} />
        </Centered>
        <Centered>
          <Button onClick={props.handleToggleModal}>Request an invite</Button>
        </Centered>
      </CallToAction>
      {modalOpen && <Modal>
        <RequestInviteForm
          submitted={submitted}
          submitting={submitting}
          touched={touched}
          hasErrors={hasErrors}
          errors={errors}
          apiError={apiError}
          handleToggleModal={props.handleToggleModal}
          handleSubmit={props.handleSubmit}
          handleUpdateField={props.handleUpdateField}
          fields={{
            email,
            emailConfirmation,
            fullname,
          }}
        />
      </Modal>
    }
    </ContentPane>,
    <Footer key={'footer'}>
      <Text>{footerContent}</Text>
    </Footer>
  ]
}

const App = () =>
  <Provider store={baseStore}>
    <AppContainer>
      <Content />
    </AppContainer>
  </Provider>

export default App;

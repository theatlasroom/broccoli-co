import * as constants from './constants'
import { validate } from '../utils/'

export const initialState = {
  initialized: false,
  apiError: null,
  errors: [],
  modalOpen: false,
  fullname: '',
  email: '',
  emailConfirmation: '',
  submitted: false,
  submitting: false,
  hasErrors: false,
  touched: false,
}

export default (state = initialState, { payload, type }) => {
  switch(type){
    case constants.INITIALIZE_SUCCESS: {
      return Object.assign({}, { ...state }, { initialized: true })
    }
    case constants.TOGGLE_MODAL: {
      return Object.assign({}, { ...state }, { modalOpen: !state.modalOpen })
    }
    case constants.VALIDATE_FIELDS: {
      // generate a new state with whatever payload we are passed
      const { errors, hasErrors } = validate(state)
      return Object.assign({}, { ...state }, { errors, hasErrors })
    }
    case constants.UPDATE_FORM_FIELD: {
      // generate a new state with whatever payload we are passed
      return Object.assign({}, { ...state }, { ...payload }, { touched: true })
    }
    case constants.REQUEST_SIGNUP:
    case constants.REQUEST_SIGNUP_SUCCESS:
    case constants.REQUEST_SIGNUP_ERROR: {
      // generate a new state with whatever payload we are passed
      return Object.assign({}, { ...state }, { ...payload })
    }
    case constants.INITIALIZE: {
      return Object.assign({}, { ...initialState })
    }
    default: {
      return state;
    }
  }
}

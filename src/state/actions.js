import { createAction } from 'redux-actions';
import * as constants from './constants'

const initialize = createAction(constants.INITIALIZE);
const initializeSuccess = createAction(constants.INITIALIZE_SUCCESS);
const toggleModal = createAction(constants.TOGGLE_MODAL);
const validateFields = createAction(constants.VALIDATE_FIELDS);
const updateFormField = createAction(constants.UPDATE_FORM_FIELD);
const requestSignup = createAction(constants.REQUEST_SIGNUP);
const requestSignupSuccess = createAction(constants.REQUEST_SIGNUP_SUCCESS);
const requestSignupError = createAction(constants.REQUEST_SIGNUP_ERROR);

export {
  initialize,
  initializeSuccess,
  toggleModal,
  validateFields,
  updateFormField,
  requestSignup,
  requestSignupSuccess,
  requestSignupError,
}

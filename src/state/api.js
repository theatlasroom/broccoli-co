import axios from 'axios';

const endpoints = {
  AUTH: '/prod/fake-auth',
}

const unwrapResponseData = resp => resp.data
const unwrapError = err => err.data.errorMessage.replace("Bad Request: ", "Error: ")

const instance = axios.create({
  baseURL: 'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/',
});

export const requestSignup = ({ resolve, reject, email, fullname: name }) => {
  const payload = { email, name }
  console.log('payload', payload);
  return instance
    .post(endpoints.AUTH, payload)
    .then(unwrapResponseData)
    .then(data => resolve(data))
    .catch(err => reject(unwrapError(err.response)))
}

import { compose, createStore, } from 'redux';
import rootReducer from './rootReducer'

export default (initialState = {}) => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // eslint-disable-line
  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(),
  )
  return store
}
